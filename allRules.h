//
// Created by mathi on 2018-11-10.
//

#ifndef LINTERPROJECT_ALLRULES_H
#define LINTERPROJECT_ALLRULES_H

#include "confFunction.h"

typedef struct BaseData
{
    char previous[1000];
    int inFunction;
    int line;
} BaseData;

void arrayBracketEol(int line, char *data, RulesElement *rulesParam, int *error, char *errorMessage);

void operatorsSpacing(int line, char *data, RulesElement *rulesParam, int *error, char *errorMessage);

void indent(int line, char *currentLine, RulesElement *rulesParam, int *error, char *errorMessage);

void maxLineNumbers(int line, char *data, RulesElement *rulesParam, int *error, char *errorMessage);

void commaSpacing(int line, char *currentLine, RulesElement *rulesParam, int *error, char *errorMessage);

void variableAssignmentType(int line, char *data, RulesElement *rulesParam, int *error, char *errorMessage);

void maxFileLineNumbers(int line, char *data, RulesElement *rulesParam, int *error, char *errorMessage);

void noTrailingSpaces(int line, char *data, RulesElement *rulesParam, int *error, char *errorMessage);

void commentsHeader(int line, char *data, RulesElement *rulesParam, int *error, char *errorMessage);
#endif //LINTERPROJECT_ALLRULES_H
